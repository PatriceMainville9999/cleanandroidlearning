package com.cleanandroidlearning.ui

import androidx.databinding.BindingAdapter
import com.akexorcist.roundcornerprogressbar.RoundCornerProgressBar

@BindingAdapter("app:roundProgressBarProgress")
fun updateProgressBar(view: RoundCornerProgressBar, value: String) {

    view.progress = value.toFloat()
}