package com.cleanandroidlearning.ui.workmanagerfun


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.cleanandroidlearning.databinding.FragmentWorkManagerFunBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class WorkManagerFunFragment : Fragment() {

    private val viewModel: WorkManagerViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentWorkManagerFunBinding.inflate(inflater, container, false)
        binding.viewmodel = viewModel
        binding.lifecycleOwner = this

        return binding.root
    }
}
