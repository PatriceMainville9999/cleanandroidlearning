package com.cleanandroidlearning.ui.workmanagerfun

import androidx.work.PeriodicWorkRequestBuilder
import androidx.work.WorkManager
import java.util.concurrent.TimeUnit

class NotificationSenderController {

    companion object {
        const val WORK_MANAGER_FUN_TAG = "WORK_MANAGER_FUN_TAG"
    }

    fun start() {
        val saveRequest = PeriodicWorkRequestBuilder<NotificationSenderWorker>(1, TimeUnit.MINUTES)
            .addTag(WORK_MANAGER_FUN_TAG)
            .build()
        WorkManager.getInstance().enqueue(saveRequest)
    }

    fun stop() {
        WorkManager.getInstance().cancelAllWorkByTag(WORK_MANAGER_FUN_TAG)
    }

}