package com.cleanandroidlearning.ui.workmanagerfun

import androidx.lifecycle.ViewModel

class WorkManagerViewModel(private val notificationSender : NotificationSenderController): ViewModel() {


    //
    // view model should just do the view presentation but not aware of context or activity
    // In our case, it just relay calls to controller that is context aware

    fun onStartPressed() {
        notificationSender.start()
    }

    fun onStopPressed() {
        notificationSender.stop()
    }
}