package com.cleanandroidlearning.ui.workmanagerfun

import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val workManagerKoinModule = module {

    single { NotificationSenderController() }
    viewModel { WorkManagerViewModel(get()) }
}