package com.cleanandroidlearning.ui.livedatafunfragment

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class LiveDataFunViewModel(private val simpleCounter: SimpleCoroutineIntCounter) : ViewModel() {

    val intCounter = MutableLiveData<String>()

    init {
        intCounter.value = "0"
    }

    fun onStartPressed() {
        simpleCounter.start { intCounter.value = it.toString() }
    }

    fun onStopPressed() {
        simpleCounter.stop()
        intCounter.value = "0"
    }

    override fun onCleared() {
        simpleCounter.stop()
    }
}