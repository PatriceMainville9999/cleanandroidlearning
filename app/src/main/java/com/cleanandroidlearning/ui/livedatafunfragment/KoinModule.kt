package com.cleanandroidlearning.ui.livedatafunfragment

import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val lifeDataFunKoinModule = module {

    single { SimpleCoroutineIntCounter() }
    viewModel { LiveDataFunViewModel(get()) }
}