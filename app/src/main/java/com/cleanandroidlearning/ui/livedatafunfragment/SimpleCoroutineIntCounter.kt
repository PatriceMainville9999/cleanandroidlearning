package com.cleanandroidlearning.ui.livedatafunfragment

import kotlinx.coroutines.*

class SimpleCoroutineIntCounter {

    private var running = false
    private var counter = 0

    fun start(callback: (Int) -> Unit) {

        running = true
        GlobalScope.launch(Dispatchers.Main) {
            while (running) {
                val cpt = async(Dispatchers.Default) { incrementCounter() }.await()
                if (cpt <= 1000) {
                    if (running) {
                        callback(cpt)
                    }
                }
                if (cpt == 1000) {
                    running = false
                }
            }
        }
    }

    private suspend fun incrementCounter(): Int {
        delay(10)
        counter += 1
        return counter
    }


    fun stop() {
        running = false
        counter = 0
    }
}