package com.cleanandroidlearning.ui.livedatafunfragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.cleanandroidlearning.databinding.FragmentLiveDataFunBinding
import org.koin.androidx.viewmodel.ext.android.viewModel


class LiveDataFunFragment : Fragment() {

    private val viewModel: LiveDataFunViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentLiveDataFunBinding.inflate(inflater, container, false)
        binding.viewmodel = viewModel
        binding.setLifecycleOwner(this)
        return binding.root
    }
}
