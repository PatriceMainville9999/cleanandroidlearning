package com.cleanandroidlearning.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.navigation.Navigation.findNavController
import com.cleanandroidlearning.R
import com.cleanandroidlearning.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FragmentHomeBinding.inflate(inflater, container, false)
        binding.lifeDataFunButton.setOnClickListener(
            Navigation.createNavigateOnClickListener(R.id.transition_to_live_data_fun_action, null)
        )

        binding.workManagerFunButton.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.workManagerFun)
        }

        binding.viewModelFunButton.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.viewModeFunFragment1)
        }

        binding.motionLayoutFun.setOnClickListener {
            Navigation.findNavController(binding.root).navigate(R.id.action_homeFragment_to_motionLayoutFun)
        }

        return binding.root
    }
}
