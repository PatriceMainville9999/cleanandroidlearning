package com.cleanandroidlearning.ui.viewmodelfun

import org.koin.androidx.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module

val viewModelFunKoinModule = module {

    viewModel { ViewModelFunViewModel() }
}