package com.cleanandroidlearning.ui.viewmodelfun

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class ViewModelFunViewModel : ViewModel() {

    val stringData = MutableLiveData<String>()

    init {
        stringData.value = "test"
    }
}