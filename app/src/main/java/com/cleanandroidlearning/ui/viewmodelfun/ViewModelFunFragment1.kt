package com.cleanandroidlearning.ui.viewmodelfun


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.cleanandroidlearning.R
import com.cleanandroidlearning.databinding.FragmentViewModeFunFragment1Binding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ViewModelFunFragment1 : Fragment() {

    private val viewModel: ViewModelFunViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentViewModeFunFragment1Binding.inflate(inflater, container, false)
        binding.navigateToNextButton.setOnClickListener {
            Navigation.findNavController(binding.root)
                .navigate(R.id.action_viewModeFunFragment1_to_viewModelFunFragment2)
        }

        binding.viewmodel = viewModel
        binding.setLifecycleOwner(this)
        return binding.root
    }
}
