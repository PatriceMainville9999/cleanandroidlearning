package com.cleanandroidlearning.ui.viewmodelfun

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.cleanandroidlearning.databinding.FragmentViewModelFunFragment2Binding
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class ViewModelFunFragment2 : Fragment() {

    private val viewModel: ViewModelFunViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentViewModelFunFragment2Binding.inflate(inflater, container, false)
        binding.navigateToPreviousButton.setOnClickListener{
            Navigation.findNavController(binding.root).navigateUp()
        }
        binding.viewmodel = viewModel
        binding.setLifecycleOwner(this)
        return binding.root
    }
}
