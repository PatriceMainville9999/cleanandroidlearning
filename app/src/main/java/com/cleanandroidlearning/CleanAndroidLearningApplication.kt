package com.cleanandroidlearning

import android.app.Application
import com.cleanandroidlearning.ui.livedatafunfragment.lifeDataFunKoinModule
import com.cleanandroidlearning.ui.viewmodelfun.viewModelFunKoinModule
import com.cleanandroidlearning.ui.workmanagerfun.workManagerKoinModule
import org.koin.android.ext.android.startKoin

class CleanAndroidLearningApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin(
            this, listOf(
                lifeDataFunKoinModule,
                workManagerKoinModule,
                viewModelFunKoinModule
            )
        )
    }
}

// @done make a test with view model sharing between fragment
// @done make a test with passing argument between fragemnt with nav controller
// @todo make a test with work manager and learn how it work
// @todo make cicleci build working... have somtehing working, but need to check artifact and test... yeah, add some test
// @todo make login with firebase
// @todo make notification channel with firebase
// @todo make chat channel with firebase
// @todo make db stuff with firebase
